��          �               %     +   3     _  %   w     �     �     �  *   �     �               +  ,   H  >   u  �  �  (   X  .   �     �  *   �     �     	       2   +     ^     v     �  )   �  ,   �  >   �    PHP version 5.3 or greater required.  WordPress version 4.7 or greater required. Boldface Future Updater Cloning of this class is not allowed. Future Revision Future Revisions Future Updater Future Updater plugin cannot be activated. Future Updates Future updater date: Nathan Johnson Update a post in the future. http://www.boldfacedesign.com/author/nathan/ http://www.boldfacedesign.com/plugins/boldface-future-updater/ Project-Id-Version: Boldface Future Updater
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-02-06 22:51+0000
PO-Revision-Date: 2017-02-06 22:52+0000
Last-Translator: nathan <nathan@boldfacedesign.com>
Language-Team: Spanish (Mexico)
Language: es-MX
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco - https://localise.biz/ Se requiere PHP versión 5.3 o superior. Se requiere WordPress versión 4.7 o superior. Boldface Futuro Actualizador No se permite la clonación de esta clase. Revisión futura Revisiones futuras Future Updater El complemento Future Updater no se puede activar. Futuras Actualizaciones Fecha de actualización futura: Nathan Johnson Actualizar una publicación en el futuro. http://www.boldfacedesign.com/author/nathan/ http://www.boldfacedesign.com/plugins/boldface-future-updater/ 