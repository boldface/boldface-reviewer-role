<?php

defined( 'ABSPATH' ) or die();

do_action( 'Boldface\FutureUpdater\functions' );

/**
 * Returns an instance of the future_revision_post_meta object for the given post ID.
 *
 * @param int $id Post ID
 *
 * @access public
 * @since 0.2
 *
 * @return object Instance of the future_revision_post_meta object
 */
if( ! function_exists( 'future_revision_post_meta' ) ):
function future_revision_post_meta( $id ) {
  return new \Boldface\FutureUpdater\methods\future_revision_post_meta( (int) $id );
}
endif;

/**
 * Returns the current priority of the current filter
 *
 * @access public
 * @since 0.2
 *
 * @return int The current priority
 */
if( ! function_exists( 'current_priority' ) ):
function current_priority() {
  global $wp_filter;
  global $wp_version;
  return version_compare( $wp_version, '4.7', '<' ) ?
    key( $wp_filter[ \current_filter() ] ) :
    $wp_filter[ \current_filter() ]->current_priority();
}
endif;

/**
 * Returns the next priority of the current filter
 *
 * @access public
 * @since 0.2
 *
 * @return int The current priority
 */
if( ! function_exists( 'next_priority' ) ):
function next_priority() {
  return \current_priority() + 1;
}
endif;

/**
 * Add an action to the current filter at the next priority
 *
 * @access public
 * @since 0.2
 */
if( ! function_exists( 'add_next_action' ) ):
function add_next_action( $callback ) {
  \add_action( \current_filter(), $callback, \next_priority() );
}
endif;
