<?php
/**
 * Plugin Name: Boldface Reviewer Role
 * Plugin URI: http://www.boldfacedesign.com/plugins/boldface-reviewer-role/
 * Description: Adds a new 'Reviewer' role and allows that role to preview draft posts.
 * Version: 0.1
 * Author: Nathan Johnson
 * Author URI: http://www.boldfacedesign.com/author/nathan/
 * Licence: GPL2+
 * Licence URI: https://www.gnu.org/licenses/gpl-2.0.en.html
 * Domain Path: /languages
 * Text Domain: boldface-reviewer-role
 */

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

//* Start bootstraping the plugin
require( dirname( __FILE__ ) . '/includes/bootstrap.php' );
$bootstrap = boldface_reviewer_role_bootstrap::getInstance( __FILE__ );
add_action( 'plugins_loaded', [ $bootstrap, 'register' ] );

//* Register activation and deactivation hooks
register_activation_hook( __FILE__ , [ $bootstrap, 'activation' ] );
register_deactivation_hook( __FILE__ , [ $bootstrap, 'deactivation' ] );
