<?php

namespace Boldface;

/**
 * Validate the namespace
 *
 * @package Boldface
 */
class namespace_validator
{
  /**
   * Look at the class and determine if it's valid
   *
   * @param string $class The class that we're attempting to load
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether or not the file is in the correct namespace
   */
  public function is_valid( $class ) {
    return ( 0 === strpos( $class, __NAMESPACE__ ) );
  }
}
