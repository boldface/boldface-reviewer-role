<?php
/**
 * Future Updater Plugin
 *
 * @package Future Updater
 */

 defined( 'ABSPATH' ) or die();

 /**
  * Class for bootstrapping the plugin
  */
class boldface_reviewer_role_bootstrap {

  /**
  * @var Main plugin file
  *
  * @access protected
  * @since 0.1
  */
  protected $file;

  /**
   * @var Error message
   *
   * @access protected
   * @since 0.2
   */
  protected $error;

  /**
   * @var object Instance of the class
   *
   * @access private
   * @since 0.1
   */
  private static $instance;

  /**
  * Constructor
  *
  * @access protected
  * @since 0.1
  */
  protected function __construct( $file ) {
    $this->file = $file;
    $this->error = __( 'Boldface Reviewer Role plugin cannot be activated.',
    'boldface-future-updater' );
  }

  /**
   * Prevent cloning of the object
   *
   * @access private
   * @since 0.3
   */
  private function __clone() {}

  /**
   * Prevent unserialization of the object
   *
   * @access public
   * @since 0.3
   */
  private function __wakeup() {}

  /**
   * Get instance of the object
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.3
   */
  public static function getInstance( $file ) {
    if( ! isset( self::$instance ) ) {
      self::$instance = new self( $file );
    }
    return self::$instance;
  }

  /**
   * Method needs to be fired on plugins_loaded
   * Checkes if minimum requirements are met and loads the plugin
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Check to see if we need to auto destruct
    //* The option is autoloaded and cached to reduce database queries
    if( false !== get_option( 'boldface_reviewer_role_error' ) ) {
      add_action( 'admin_notices', [ $this, 'self_destruct_error' ] );
      add_action( 'admin_notices', [ $this, 'self_destruct' ], 15 );

      return;
    }

    //* Autoloader
    require( __DIR__ . '/autoload.php' );
    spl_autoload_register( array( new \Boldface\autoload(), 'load' ) );

    //* Include functions file
    $functions = \trailingslashit( WP_PLUGIN_DIR ) .
      \plugin_basename( \plugin_dir_path( $this->file ) ) . '/functions.php';
    if( ! file_exists( $functions ) ) {
      wp_die( __( $functions . ' does not exists.', 'boldface-future-updater' ) );
    }
    require_once( $functions );

    do_action( 'ReviewerRole\init' );

    //* Add action to the current hook at the next priority
    $plugin = \Boldface\ReviewerRole\plugin::getInstance( $this->file );
    \add_next_action( [ $plugin, 'register' ] );
  }

  /**
   * Add action on activation to maybe_self_destruct
   *
   * @access public
   * @since 0.1
   */
  public function activation() {
    \add_action( 'shutdown', [ $this, 'maybe_self_destruct' ] );

    require_once(
      dirname( $this->file ) . '/includes/reviewerrole/methods/roles.php' );
    ( new \Boldface\ReviewerRole\Methods\roles() )->add();
  }

  /**
   * Fired on plugin deactivation
   *
   * @access public
   * @since 0.1
   */
  public function deactivation() {
    //* We intentionally don't cleanup options added to the database, so that the
    //* plugin can be re-activated and resume operation as if it wasn't deactivated.
    \delete_option( 'boldface_reviewer_role_error' );

    require_once(
      dirname( $this->file ) . '/includes/reviewerrole/methods/roles.php' );
    ( new \Boldface\ReviewerRole\Methods\roles() )->remove();
  }

  /**
   * Checkes if minimum requirements are met and adds autoloading option
   *
   * @access public
   * @since 0.1
   */
  public function maybe_self_destruct() {
    //* Check version.
    //* Add autoloaded option to database to see if we need to self-destruct
    \delete_option( 'boldface_reviewer_role_error' );
    if( ! $this->version_check() ) {
      \update_option( 'boldface_reviewer_role_error', $this->error, '', 'yes' );
    }
  }

  /**
   * Self destruct by deactivating the plugin
   *
   * @access public
   * @since 0.1
   */
  public function self_destruct() {
    \deactivate_plugins( $this->file );
  }

  /**
   * Error message to display on self destruct
   *
   * @access public
   * @since 0.1
   */
  public function self_destruct_error() {
    printf( '
      <div class="error"><p>%1$s</p></div>
      <style>#message.updated{ display: none; }</style>',
      get_option( 'boldface_reviewer_role_error' ) );
  }

  /**
  * Version check
  *
  * @access protected
  * @since 0.1
  *
  * @return bool Whether the version check pass or fail
  */
  protected function version_check() {
    return $this->php_version_check() && $this->wp_version_check();
  }

  /**
  * Check the PHP version
  * The main plugin file and this one will work with PHP 5.2. Other files
  * require at least PHP 5.3.
  *
  * @access protected
  * @since 0.1
  *
  * @return bool Whether the PHP version is greater than 5.3
  */
  protected function php_version_check() {
    if( version_compare( PHP_VERSION, '5.3', '<' ) ) {
      $this->error .= __( ' PHP version 5.3 or greater required.',
        'boldface-future-updater' );
        return false;
    }
    return true;
  }

  /**
  * Check the WordPress version. Requires WP 4.7 or greater.
  *
  * @access protected
  * @since 0.2
  *
  * @return bool Whether the WordPress version is greater than 4.7
  */
  protected function wp_version_check() {
    global $wp_version;
    if( version_compare( $wp_version, '4.7', '<' ) ) {
      $this->error .= __( ' WordPress version 4.7 or greater required.',
        'boldface-future-updater' );
        return false;
    }
    return true;
  }
}
