<?php
/**
 * Future Updater Plugin
 *
 * @package Future Updater
 */
namespace Boldface\ReviewerRole;

use \Boldface\ReviewerRole\hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for loading the plugin
 */
class plugin {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected $file;

  /**
   * @var object Instance of the class
   *
   * @access private
   * @since 0.1
   */
  private static $instance;

  /**
  * Constructor
  *
  * @access public
  * @since 0.1
  */
  protected function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Prevent cloning of the object
   *
   * @access private
   * @since 0.1
   */
  private function __clone() {}

  /**
   * Prevent unserialization of the object
   *
   * @access public
   * @since 0.1
   */
  private function __wakeup() {}

  /**
   * Get instance of the object
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public static function getInstance( $file ) {
    if( ! isset( self::$instance ) ) {
      self::$instance = new self( $file );
    }
    return self::$instance;
  }

  /**
   * Add action to load the plugin.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Fires on initiation to initialize the plugin
    \add_action( 'init',
      [ hooks\init::getInstance( $this->file ), 'register' ], 5 );
  }
}
