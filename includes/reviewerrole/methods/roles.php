<?php
/**
 * Roles API
 *
 * @package Reviewer Role
 */
namespace Boldface\ReviewerRole\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for interacting with WordPress roles
 */
class roles {

  /**
   * @var array Default arguments
   *
   * @access public
   * @since 0.2
   */
  protected $args = [
    'reviewer' => [
      'name' => 'Reviewer',
      'caps' => [
        'read'                    => true,
        'edit_posts'              => true,
        'read_others_draft_posts' => true,
      ],
    ],
  ];

  /**
   * Add Roles
   *
   * @param array $args Array of arguments
   *
   * @access public
   * @since 0.2
   */
  public function add( $args = [] ) {
    $args = \wp_parse_args( $args, $this->args );
    foreach( $args as $role => $role_args ) {
      \add_role( $role, $role_args[ 'name' ], $role_args[ 'caps' ] );
    }
  }

  /**
   * Remove roles
   *
   * @param array $args Array of arguments
   *
   * @access public
   * @since 0.2
   */
  public function remove( $args = [] ) {
    $args = \wp_parse_args( $args, $this->args );
    foreach( $args as $role => $caps ) {
      \remove_role( $role );
    }
  }
}
