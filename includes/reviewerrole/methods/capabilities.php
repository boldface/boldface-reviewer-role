<?php
/**
 * Capabilities API
 *
 * @package Reviewer Role
 */
namespace Boldface\ReviewerRole\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for interacting with WordPress capabilities
 */
class capabilities {

  /**
   * @var array Default arguments
   *
   * @access public
   * @since 0.2
   */
  protected $args = [
    'editor' => [
      'edit_future-revisions',
    ],
    'administrator' => [
      'edit_future-revisions',
      'delete_future-revisions',
    ],
  ];

  /**
   * Add capabilities
   *
   * @param array $args Array of arguments
   *
   * @access public
   * @since 0.2
   */
  public function add( $args = [] ) {
    $args = \wp_parse_args( $args, $this->args );
    foreach( $args as $role => $caps ) {
      $role = get_role( $role );
      foreach( $caps as $cap ) {
        $role->add_cap( $cap );
      }
    }
  }

  /**
   * Remove capabilities
   *
   * @param array $args Array of arguments
   *
   * @access public
   * @since 0.2
   */
  public function remove( $args = [] ) {
    $args = \wp_parse_args( $args, $this->args );
    foreach( $args as $role => $caps ) {
      $role = get_role( $role );
      foreach( $caps as $cap ) {
        $role->remove_cap( $cap );
      }
    }
  }
}
