<?php
/**
 * Hooks\load_edit
 *
 * @package ReviewerRole
 */
namespace Boldface\ReviewerRole\Hooks;

use \Boldface\ReviewerRole\hooks as hooks;
use \Boldface\ReviewerRole\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the post edit screens
 */
class load_edit {

  /**
   * @var string $post_type The post type
   *
   * @access protected
   * @since 0.1
   */
  protected $post_type;

  /**
   * @var object $methods Methods for load_edit
   *
   * @access protected
   * @since 0.1
   */
  protected $methods;

  /**
   * @var object Instance of the class
   *
   * @access private
   * @since 0.1
   */
  private static $instance;

  /**
   * Object constructor
   *
   * @access protected
   * @since 0.2
   */
  protected function __construct( $post_type ) {
    $this->post_type = $post_type;
    //$this->methods = methods\load_edit::getInstance();
  }

  /**
   * Prevent cloning of the object
   *
   * @access private
   * @since 0.3
   */
  private function __clone() {}

  /**
   * Prevent unserialization of the object
   *
   * @access public
   * @since 0.3
   */
  private function __wakeup() {}

  /**
   * Get instance of the object
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.3
   */
  public static function getInstance( $post_type ) {
    if( ! isset( self::$instance ) ) {
      self::$instance = new self( $post_type );
    }
    return self::$instance;
  }

  /**
   * Add actions to hooks
   *
   * @access public
   * @since 0.1
   */
  public function register() {
  }
}
