<?php
/**
 * Hooks/current_screen
 *
 * @package Reviewer Role
 */
namespace Boldface\ReviewerRole\Hooks;

use \Boldface\ReviewerRole\hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress current_screen
 */
class current_screen {

  /**
   * Use the current screen to get the post_type. And add hooks.
   *
   * @access public
   * @since 0.2
   */
  public function register() {

    $post_type = ( get_current_screen() )->post_type;

    \add_action( 'load-edit.php',
      [ hooks\load_edit::getInstance( $post_type ), 'register' ] );
  }
}
