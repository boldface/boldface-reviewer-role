<?php
/**
 * Hooks/init
 *
 * @package Reviewer Role
 */
namespace Boldface\ReviewerRole\Hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress public initiation
 */
class public_init {

  /**
   * @var object Instance of the class
   *
   * @access private
   * @since 0.1
   */
  private static $instance;

  /**
   * Object constructor
   *
   * @access protected
   * @since 0.1
   */
  protected function __construct() {
  }

  /**
   * Prevent cloning of the object
   *
   * @access private
   * @since 0.1
   */
  private function __clone() {}

  /**
   * Prevent unserialization of the object
   *
   * @access public
   * @since 0.1
   */
  private function __wakeup() {}

  /**
   * Get instance of the object
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public static function getInstance() {
    if( ! isset( self::$instance ) ) {
      self::$instance = new self;
    }
    return self::$instance;
  }

  /**
   * Add action that fires on the_post.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    \add_filter( 'user_has_cap', function( $allcaps, $cap, $args ) {
      if ( 'edit_post' != $args[0] ) {
        return $allcaps;
      }

      if( ! isset( $allcaps[ 'read_others_draft_posts' ] ) ) {
        return $allcaps;
      }

      $post = get_post( $args[2] );
      if( 'draft' !== $post->post_status ) {
        return $allcaps;
      }
      $allcaps[ $cap[ 0 ] ] = true;
      return $allcaps;
    }, 10, 3 );

  }
}
