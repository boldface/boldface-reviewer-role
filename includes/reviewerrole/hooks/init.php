<?php
/**
 * Hooks/init
 *
 * @package Reviewer Role
 */
namespace Boldface\ReviewerRole\Hooks;

use \Boldface\ReviewerRole\methods as methods;
use \Boldface\ReviewerRole\hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress initiation
 */
class init {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected $file;

  /**
   * @var object Instance of the class
   *
   * @access private
   * @since 0.1
   */
  private static $instance;

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Prevent cloning of the object
   *
   * @access private
   * @since 0.1
   */
  private function __clone() {}

  /**
   * Prevent unserialization of the object
   *
   * @access public
   * @since 0.1
   */
  private function __wakeup() {}

  /**
   * Get instance of the object
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public static function getInstance( $file ) {
    if( ! isset( self::$instance ) ) {
      self::$instance = new self( $file );
    }
    return self::$instance;
  }

  /**
   * Add action to initialize the plugin
   *
   * @access public
   * @since 0.1
   */
  public function register() {

    //* Fires on initiation to load plugin text domain
    \add_action( 'init',
      [ new methods\load_textdomain( $this->file ), 'register' ] );

    if( is_admin() ) {
      //* Fires on admin_init to load hooks only for admin
      //\add_action( 'admin_init',
      //  [ new hooks\admin_init( $this->file ), 'register' ], 5 );
      return;
    }
    \add_action( 'init',
      [ hooks\public_init::getInstance(), 'register' ], 1000 );
  }
}
