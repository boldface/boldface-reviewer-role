<?php
/**
 * Hooks/admin_init
 *
 * @package Reviewer Role
 */
namespace Boldface\ReviewerRole\Hooks;

use \Boldface\ReviewerRole\hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress admin initiation
 */
class admin_init {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected $file;

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Add several actions to load the admin.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Fires on current_screen to modify the load-edit screen
    \add_action( 'current_screen', [ new hooks\current_screen(), 'register' ] );
  }
}
