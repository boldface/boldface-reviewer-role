=== Boldface Reviewer Role ===
Contributors: NathanAtmoz
Requires at least: 4.7
Tested up to: 4.7.2
Stable tag: trunk
License: GPL2+
License URI: https://www.gnu.org/licenses/gpl-2.0.en.html

Creates a role that has the ability to preview draft posts on the public interface.

== Description ==
Creates a role that has the ability to preview draft posts on the public interface.

== Installation ==
Download zip file to the wp-plugins folder and extract. Activate from plugins screen.

== Changelog ==
=== 0.2 ===
* Update APIs

=== 0.1 ===
* Initial release
