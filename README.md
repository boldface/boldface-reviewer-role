# Boldface Reviewer Role

## Description
Creates a role that has the ability to preview draft posts on the public interface.

## Requirements
This plugin has been tested with WordPress 4.7. It requires PHP 5.3 or greater.

## Installation
Download the plugin zip file to your WordPress plugins directory. Extract the
files. Activate the plugin.
